
CREATE TABLE library
(
    id   INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL UNIQUE ,
    location VARCHAR(255),
    phone VARCHAR(255),
    PRIMARY KEY (id)
);

CREATE TABLE book
(
    id   INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255),
    library_id INT NOT NULL,
    CONSTRAINT FOREIGN KEY (library_id) REFERENCES library (id),
    book_details_id INT NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE author
(
    id   INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255),
    surname VARCHAR(255),
    email VARCHAR(255) NOT NULL UNIQUE,
    PRIMARY KEY (id)
);

CREATE TABLE book_details
(
    id   INT NOT NULL AUTO_INCREMENT,
    description VARCHAR(255),
    isbn VARCHAR(255),
    page_count INT,
    book_id INT NOT NULL,
    CONSTRAINT FOREIGN KEY (book_id) REFERENCES book (id),
    PRIMARY KEY (id)
);

ALTER TABLE book ADD FOREIGN KEY (book_details_id) REFERENCES book_details (id);

CREATE TABLE book_author_rel
(
    book_id INT NOT NULL,
    author_id INT NOT NULL,
    CONSTRAINT FOREIGN KEY (book_id) REFERENCES book (id),
    CONSTRAINT FOREIGN KEY (author_id) REFERENCES author (id)
);
