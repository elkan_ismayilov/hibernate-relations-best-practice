CREATE TABLE book_author_rel
(
    book_id INT NOT NULL,
    author_id INT NOT NULL,
    CONSTRAINT FOREIGN KEY (book_id) REFERENCES book (id),
    CONSTRAINT FOREIGN KEY (author_id) REFERENCES author (id)
);