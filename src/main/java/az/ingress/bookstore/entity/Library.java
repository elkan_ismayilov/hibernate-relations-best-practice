package az.ingress.bookstore.entity;

import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;

/**
 * book-store
 * Elkhan
 * 15.02.2024 22:38
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "library")
public class Library implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "name", nullable = false, length = 50, unique = true)
    private String name;
    private String location;
    private String phone;

}
