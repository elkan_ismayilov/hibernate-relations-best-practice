package az.ingress.bookstore.entity;

import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;

/**
 * book-store
 * Elkhan
 * 15.02.2024 22:57
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "book_details")
public class BookDetails implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String description;
    private String isbn;
    @Column(name = "page_count")
    private Integer pageCount;
}
