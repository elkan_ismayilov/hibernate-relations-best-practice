package az.ingress.bookstore.repository;

import az.ingress.bookstore.entity.Library;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * book-store
 * Elkhan
 * 15.02.2024 23:10
 */
public interface LibraryRepository extends JpaRepository<Library, Integer> {
}
