package az.ingress.bookstore.repository;

import az.ingress.bookstore.entity.BookDetails;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * book-store
 * Elkhan
 * 16.02.2024 07:30
 */
public interface BookDetailsRepository extends JpaRepository<BookDetails, Integer> {
}
