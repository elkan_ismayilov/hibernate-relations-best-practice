package az.ingress.bookstore;

import az.ingress.bookstore.entity.*;
import az.ingress.bookstore.repository.BookDetailsRepository;
import az.ingress.bookstore.repository.BookRepository;
import az.ingress.bookstore.repository.LibraryRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.List;
import java.util.Set;

@SpringBootApplication
public class BookStoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(BookStoreApplication.class, args);
    }

    @Bean
    CommandLineRunner init(BookRepository bookRepository,
                           LibraryRepository libraryRepository) {
        return args -> {
            Library library = libraryRepository.findById(8).orElseThrow(() -> new RuntimeException("Library not found"));
            BookDetails bookDetails = BookDetails.builder()
                    .description("Description")
                    .isbn("4213")
                    .pageCount(100)
                    .build();
            Set<Author> authors = Set.of(

                    Author.builder()
                    .name("Author 1")
                    .surname("Author Surname 1")
                    .email("author1@mail")
                    .build(),

                    Author.builder()
                            .name("Author 2")
                            .surname("Author Surname 2")
                            .email("author2@mail")
                            .build()

            );

            Book book = Book.builder()
                    .name("Book")
                    .library(library)
                    .bookDetails(bookDetails)
                    .authors(authors)
                    .build();

            bookRepository.save(book);
//            List<Book> books = bookRepository.findAllById(List.of(13));
//            bookRepository.deleteAll(books);
        };
    }

}
